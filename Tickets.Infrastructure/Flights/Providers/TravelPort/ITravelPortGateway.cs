﻿using Tickets.Domain.Flights.DomainRequest;
using Tickets.Domain.Flights.Entities;

namespace Tickets.Infrastructure.Flights.Providers.TravelPort;

/// <summary>
/// Fake интеграция с сервисом Travelport
/// https://support.travelport.com/webhelp/uapi/uAPI.htm
/// </summary>
public interface ITravelPortGateway
{
    Task<IEnumerable<Flight>> FindAsync(GetFlightsDomainQuery query);
}