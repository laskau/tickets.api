﻿using Microsoft.Extensions.Logging;
using Tickets.Infrastructure.HttpUtils.RequestAndResponseLogEntries;

namespace Tickets.Infrastructure.HttpUtils;

public sealed class RequestResponseMessagesLoggingHandler(
    IHttpClientMessageLogEntryFactory httpClientMessageLogEntryFactory,
    ILogger<RequestResponseMessagesLoggingHandler> logger)
    : DelegatingHandler
{
    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
        var requestLogEntry = await httpClientMessageLogEntryFactory.CreateAsync(request);
        LogRequestAsync(requestLogEntry);

        var response = await base.SendAsync(request, cancellationToken);
        var responseLogEntry = await httpClientMessageLogEntryFactory.CreateAsync(response);
        LogResponseAsync(responseLogEntry);

        return response;
    }

    private void LogRequestAsync(HttpRequestMessageLogEntry logEntry)
    {
        logger.LogInformation("Request[{@httpMethod} {@uri} {@content}]",
            logEntry.HttpMethod,
            logEntry.Uri,
            logEntry.Content);
    }

    private void LogResponseAsync(HttpResponseMessageLogEntry logEntry)
    {
        logger.LogInformation("Response[{@httpMethod} {@uri} {@httpStatusCode} {@content}]",
            logEntry.HttpMethod,
            logEntry.Uri,
            logEntry.StatusCode,
            logEntry.Content);
    }
}