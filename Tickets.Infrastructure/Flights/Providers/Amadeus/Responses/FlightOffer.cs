﻿using System.Text.Json.Serialization;

namespace Tickets.Infrastructure.Flights.Providers.Amadeus.Responses;

public sealed class FlightOffer
{
    [JsonPropertyName("itineraries")]
    public Itineraries[] Itineraries { get; set; }
}

public sealed class Itineraries
{
    [JsonPropertyName("segments")]
    public FlightSegment[] Segments { get; set; }
}

public sealed class FlightSegment
{

    [JsonPropertyName("number")]
    public string Number { get; set; }
    [JsonPropertyName("departure")]
    public FlightSegmentPoint Departure { get; set; }
    [JsonPropertyName("arrival")]
    public FlightSegmentPoint Arrival { get; set; }
}

public sealed class FlightSegmentPoint
{
    [JsonPropertyName("iataCode")]
    public string IATACode { get; set; }
    [JsonPropertyName("at")]
    public DateTime At { get; set; }
}