﻿using Tickets.Domain.Airports.Entities;

namespace Tickets.Domain.Airports;

public interface IAirportsRepository
{
    Task<Airport[]> GetAsync();
}