﻿namespace Tickets.API.Application.Flights.Models;

public sealed class FlightModel
{
    public string FlightNumber { get; set; }
    public DepartureModel Departure { get; set; }
    public ArrivalModel Arrival { get; set; }
}

public sealed class DepartureModel
{
    public DateTimeOffset DepartureDateUtc { get; init; }
    public string AirportName { get; init; }
    public string AirportCode { get; init; }
    public string CityName { get; init; }
}

public sealed class ArrivalModel
{
    public DateTimeOffset ArrivalDateUtc { get; init; }
    public string AirportName { get; init; }
    public string AirportCode { get; init; }
    public string CityName { get; init; }
}
