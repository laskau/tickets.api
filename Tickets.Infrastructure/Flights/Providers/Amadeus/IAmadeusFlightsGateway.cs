﻿using Tickets.Domain.Flights.DomainRequest;
using Tickets.Domain.Flights.Entities;

namespace Tickets.Infrastructure.Flights.Providers.Amadeus;
/// <summary>
/// Fake интеграция с сервисом Amadeus
/// https://developers.amadeus.com/self-service/category/flights/api-doc/flight-offers-search/api-reference
/// </summary>
public interface IAmadeusFlightsGateway
{
    Task<IEnumerable<Flight>> SearchAsync(GetFlightsDomainQuery query);
}