﻿using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace Tickets.Infrastructure.SoapUtils;

public sealed class HttpMessageHandlerBehavior : IEndpointBehavior
{
    private readonly Func<HttpMessageHandler> _httpMessageHandler;

    public HttpMessageHandlerBehavior(IHttpMessageHandlerFactory factory, string soapHttpClientName)
    {
        _httpMessageHandler = () => factory.CreateHandler(soapHttpClientName);
    }

    public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
    {
        bindingParameters.Add(new Func<HttpClientHandler, HttpMessageHandler>(handler => _httpMessageHandler()));
    }

    public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime) { }

    public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher) { }

    public void Validate(ServiceEndpoint endpoint) { }
}