﻿namespace Tickets.Infrastructure.HttpUtils.RequestAndResponseLogEntries;

public interface IHttpClientMessageLogEntryFactory
{
    Task<HttpRequestMessageLogEntry> CreateAsync(HttpRequestMessage requestMessage);
    Task<HttpResponseMessageLogEntry> CreateAsync(HttpResponseMessage responseMessage);
}