﻿using System.ServiceModel;
using System.Text;
using Tickets.Domain.Flights.DomainRequest;
using Tickets.Domain.Flights.Entities;
using Tickets.Infrastructure.SoapUtils;
using Travelport;

namespace Tickets.Infrastructure.Flights.Providers.TravelPort;

public class TravelPortGateway(string endpoint, TimeSpan timeOut, IHttpMessageHandlerFactory httpHandlerFactory)
    : ITravelPortGateway
{
    public AvailabilitySearchReq CreateSearchRequest(GetFlightsDomainQuery query) => new()
    {
        Items =
        [
            new SearchAirLeg()
            {
                SearchOrigin = [new typeSearchLocation() { Item = new Airport() { Code = query.DepartureAirport.IATACode } }],
                SearchDestination = [new typeSearchLocation() {Item = new Airport() {Code = query.ArrivalAirport.IATACode}}],
                Items = [new typeFlexibleTimeSpec(){Item = query.FlightDate}],
                AirLegModifiers = new AirLegModifiers(){ FlightType = new FlightType() { MaxConnections = "0", MaxStops = "1", RequireSingleCarrier = true} }
            },
        ]
    };

    private AirAvailabilitySearchPortTypeClient CreateSoapClient()
    {
        var endpointAddress = new EndpointAddress(endpoint);
        var basicHttpBinding = new BasicHttpBinding(BasicHttpSecurityMode.None)
        {
            OpenTimeout = timeOut,
            CloseTimeout = timeOut,
            SendTimeout = timeOut,
            ReceiveTimeout = timeOut,
            TextEncoding = Encoding.UTF8
        };

        var client = new AirAvailabilitySearchPortTypeClient(basicHttpBinding, endpointAddress);
        client.Endpoint.EndpointBehaviors.Add(new HttpMessageHandlerBehavior(httpHandlerFactory, "TravelPort"));

        return client;
    }

    public async Task<IEnumerable<Flight>> FindAsync(GetFlightsDomainQuery query)
    {
        var client = CreateSoapClient();

        var response = await client.serviceAsync(default, CreateSearchRequest(query));

        var availableSegments = response.AvailabilitySearchRsp.AirSegmentList;

        return availableSegments.Select(s => new Flight()
        {
            FlightNumber = s.FlightNumber,
            Arrival = query.ArrivalAirport,
            ArrivalDate = DateTime.Parse(s.ArrivalTime),
            Departure = query.DepartureAirport,
            DepartureDate = DateTime.Parse(s.DepartureTime)
        });
    }
}