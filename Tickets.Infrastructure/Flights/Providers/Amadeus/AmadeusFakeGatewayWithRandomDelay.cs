﻿using Tickets.Domain.Flights.DomainRequest;
using Tickets.Domain.Flights.Entities;

namespace Tickets.Infrastructure.Flights.Providers.Amadeus;

public sealed class AmadeusFakeGatewayWithRandomDelay : IAmadeusFlightsGateway
{
    public async Task<IEnumerable<Flight>> SearchAsync(GetFlightsDomainQuery query)
    {
        var departureDate = query.FlightDate.AddHours(Random.Shared.Next(3, 5));

        await Task.Delay(TimeSpan.FromMilliseconds(Random.Shared.Next(0, 310)));

        return Enumerable.Range(1, Random.Shared.Next(5))
            .Select(_ => new Flight()
            {
                FlightNumber = $"AMADEUS-{Random.Shared.Next(9999, 99999)}",
                Departure = query.DepartureAirport,
                DepartureDate = departureDate,
                Arrival = query.ArrivalAirport,
                ArrivalDate = departureDate.AddHours(Random.Shared.Next(5, 8))
            }).ToArray();
    }
}