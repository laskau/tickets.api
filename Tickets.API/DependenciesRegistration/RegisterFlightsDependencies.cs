﻿using FluentValidation;
using Microsoft.Extensions.Caching.Memory;
using Tickets.API.Application.Flights.Requests;
using Tickets.API.Application.Flights.Requests.Validators;
using Tickets.Domain.Flights;
using Tickets.Infrastructure.Flights;
using Tickets.Infrastructure.Flights.Providers.Database;
using Tickets.Infrastructure.Flights.Settings;

namespace Tickets.API.DependenciesRegistration;

public static class RegisterFlightsDependencies
{
    public static IServiceCollection AddFlightsDependencies(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<FlightsAggregatorSettings>(configuration.GetSection("FlightsAggregatorSettings"));

        services.AddScoped<IValidator<GetFlightsRequest>, GetFlightsRequestValidator>();
        services.AddScoped<IFlightsDbRepository, FlightsFakeRepositoryWithRandomDelay>();

        //services.AddScoped<IFlightsDbRepository, FlightsDbRepository>(s => new FlightsDbRepository(
        //    configuration.GetValue<string>("ConnectionString:FlightsDb"),
        //    s.GetRequiredService<IAirportsRepository>()));

        services.AddScoped<FlightsAggregatorService>();
        services.AddScoped<IFlightsAggregatorService>(s => new FlightsAggregatorCachingProxy(
            s.GetRequiredService<FlightsAggregatorService>(),
            s.GetRequiredService<IMemoryCache>()));

        return services;
    }
}