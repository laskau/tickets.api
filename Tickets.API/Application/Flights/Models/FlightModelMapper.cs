﻿using Tickets.Domain.Flights.Entities;

namespace Tickets.API.Application.Flights.Models;

public static class FlightModelMapper
{
    public static FlightModel Map(Flight flight) => new()
    {
        FlightNumber = flight.FlightNumber,
        Departure = new DepartureModel()
        {
            AirportCode = flight.Departure.IATACode,
            AirportName = flight.Departure.Name,
            CityName = flight.Departure.City,
            DepartureDateUtc = new DateTimeOffset(flight.DepartureDate, flight.Departure.TimeZone.BaseUtcOffset)
        },
        Arrival = new ArrivalModel()
        {
            AirportCode = flight.Arrival.IATACode,
            AirportName = flight.Arrival.Name,
            CityName = flight.Arrival.City,
            ArrivalDateUtc = new DateTimeOffset(flight.ArrivalDate, flight.Arrival.TimeZone.BaseUtcOffset)
        }
    };
}