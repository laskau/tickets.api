﻿namespace Tickets.Domain.Airports.Entities;

public class Airport
{
    public virtual string IATACode { get; set; }
    public string Name { get; set; }
    public string Country { get; set; }
    public string City { get; set; }
    public TimeZoneInfo TimeZone { get; set; }
}