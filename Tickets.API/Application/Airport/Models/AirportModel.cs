﻿namespace Tickets.API.Application.Airport.Models;

public sealed class AirportModel
{
    public string Name { get; init; }
    public string Code { get; init; }
    public string Country { get; init; }
}