﻿using Tickets.Domain.Airports.Entities;

namespace Tickets.Domain.Flights.Entities;

public sealed class Flight
{
    public string FlightNumber { get; set; }
    public Airport Departure { get; set; }
    public DateTime DepartureDate { get; set; }
    public Airport Arrival { get; set; }
    public DateTime ArrivalDate { get; set; }
}