﻿using System.Text.Json;
using Tickets.Domain.Flights.DomainRequest;
using Tickets.Domain.Flights.Entities;
using Tickets.Infrastructure.Flights.Providers.Amadeus.Responses;

namespace Tickets.Infrastructure.Flights.Providers.Amadeus;

public sealed class AmadeusFlightsGateway(HttpClient httpClient) : IAmadeusFlightsGateway
{
    private IEnumerable<Flight> Map(FlightOffer result, GetFlightsDomainQuery query) => result.Itineraries
        .TakeWhile(itinerary => itinerary.Segments.Length == 1)
        .Select(itinerary => new Flight()
        {
            FlightNumber = itinerary.Segments.First().Number,
            Departure = query.DepartureAirport,
            DepartureDate = itinerary.Segments.First().Departure.At,
            Arrival = query.ArrivalAirport,
            ArrivalDate = itinerary.Segments.First().Arrival.At
        });

    public async Task<IEnumerable<Flight>> SearchAsync(GetFlightsDomainQuery query)
    {
        var message = new HttpRequestMessage(HttpMethod.Get, $"shopping/flight-offers" +
                                                             $"?originLocationCode={query.DepartureAirport.IATACode}" +
                                                             $"&destinationLocationCode={query.ArrivalAirport.IATACode}" +
                                                             $"&departureDate={query.FlightDate:yyyy-MM-dd}" +
                                                             $"&nonStop=true");

        var response = await httpClient.SendAsync(message);
        response.EnsureSuccessStatusCode();
        var content = await response.Content.ReadAsStringAsync();

        var offer = JsonSerializer.Deserialize<FlightOffer>(content);

        return Map(offer, query);
    }
}