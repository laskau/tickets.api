﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Tickets.Domain.Flights;
using Tickets.Domain.Flights.DomainRequest;
using Tickets.Domain.Flights.Entities;
using Tickets.Infrastructure.Flights.Providers.Amadeus;
using Tickets.Infrastructure.Flights.Providers.Database;
using Tickets.Infrastructure.Flights.Providers.TravelPort;
using Tickets.Infrastructure.Flights.Settings;

namespace Tickets.Infrastructure.Flights;

public class FlightsAggregatorService(
    IFlightsDbRepository flightRepository,
    IAmadeusFlightsGateway amadeusGateway,
    ITravelPortGateway travelPortGateway,
    IOptionsMonitor<FlightsAggregatorSettings> settings,
    ILogger<FlightsAggregatorService> logger) : IFlightsAggregatorService
{
    private async Task<IEnumerable<Flight>> GetFromAmadeusSafelyAsync(GetFlightsDomainQuery query)
    {
        try
        {
            return await amadeusGateway.SearchAsync(query);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Error occurred when accessing Amadeus API");
            return [];
        }
    }
    private async Task<IEnumerable<Flight>> GetFromTravelPortSafelyAsync(GetFlightsDomainQuery query)
    {
        try
        {
            return await travelPortGateway.FindAsync(query);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Error occurred when accessing TravelPort API");
            return [];
        }
    }

    private async Task<IEnumerable<Flight>> GetFromDbSafelyAsync(GetFlightsDomainQuery query)
    {
        try
        {
            return await flightRepository.GetAsync(query.DepartureAirport.IATACode, query.ArrivalAirport.IATACode, query.FlightDate);
        }
        catch (Exception e)
        {
            logger.LogError(e, "Error occurred when accessing TravelPort API");
            return [];
        }
    }

    public async Task<Flight[]> GetAsync(GetFlightsDomainQuery query)
    {
        var timeoutTrigger = Task.Delay(settings.CurrentValue.ServiceTimeout)
            .ContinueWith(_ => Enumerable.Empty<Flight>());
        var tasks = new[]
            { GetFromAmadeusSafelyAsync(query), GetFromTravelPortSafelyAsync(query), GetFromDbSafelyAsync(query) };

        var completedTasks = (await Task.WhenAll(tasks.Select(task => Task.WhenAny(task, timeoutTrigger))))
            .Where(task => task != timeoutTrigger).ToArray();

        var collectionOfResults = await Task.WhenAll(completedTasks);

        return collectionOfResults.SelectMany(x => x).ToArray();
    }
}