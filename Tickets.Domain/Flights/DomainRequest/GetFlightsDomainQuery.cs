﻿using Tickets.Domain.Airports.Entities;

namespace Tickets.Domain.Flights.DomainRequest;

public class GetFlightsDomainQuery
{
    public Airport DepartureAirport { get; set; }
    public Airport ArrivalAirport { get; set; }
    public DateTime FlightDate { get; set; }
}