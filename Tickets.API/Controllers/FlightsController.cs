using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Tickets.API.Application.Flights.Models;
using Tickets.API.Application.Flights.Requests;
using Tickets.Domain.Airports;
using Tickets.Domain.Flights;
using Tickets.Domain.Flights.DomainRequest;

namespace Tickets.API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class FlightsController : ControllerBase
{
    [HttpGet]
    public async Task<IActionResult> Get([FromQuery] GetFlightsRequest request,
        [FromServices] IValidator<GetFlightsRequest> validator,
        [FromServices] IAirportsRepository airportsRepository,
        [FromServices] IFlightsAggregatorService aggregator)
    {
        var validationResult = await validator.ValidateAsync(request);
        if (!validationResult.IsValid)
        {
            validationResult.AddToModelState(ModelState);
            return BadRequest(ModelState);
        }

        var airports = await airportsRepository.GetAsync();
        var query = new GetFlightsDomainQuery()
        {
            DepartureAirport = airports.First(a => a.IATACode.Equals(request.From, StringComparison.OrdinalIgnoreCase)),
            ArrivalAirport = airports.First(a => a.IATACode.Equals(request.To, StringComparison.OrdinalIgnoreCase)),
            FlightDate = request.Date
        };
        var result = await aggregator.GetAsync(query);

        if (result.Length == 0)
            return NotFound();

        return Ok(result.Select(FlightModelMapper.Map));
    }
}