﻿using Polly;
using Polly.CircuitBreaker;
using System.Threading.RateLimiting;
using Tickets.Infrastructure.Flights.Providers.TravelPort;

namespace Tickets.API.DependenciesRegistration;

public static class RegisterTravelPortDependencies
{
    public static IServiceCollection AddTravelPortDependencies(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddScoped<ITravelPortGateway, TravelPortFakeGatewayWithRandomDelay>()
            .AddTransient<TravelPortGateway>(s =>
            {
                var timeOut = TimeSpan.FromMilliseconds(configuration.GetValue<int>("AppSettings:TravelPortTimeoutInMs"));
                var endpoint = configuration.GetValue<string>("AppSettings:TravelPortEndpoint");
                var factory = s.GetRequiredService<IHttpMessageHandlerFactory>();

                return new TravelPortGateway(endpoint, timeOut, factory);
            })
            .AddResiliencePipeline("TravelPortPipeline", pipelineBuilder =>
            {
                pipelineBuilder.AddCircuitBreaker(new CircuitBreakerStrategyOptions()
                {
                    SamplingDuration = TimeSpan.FromSeconds(30),
                    FailureRatio = 0.2,
                    MinimumThroughput = 3,
                });

                pipelineBuilder.AddRateLimiter(new SlidingWindowRateLimiter(
                    new SlidingWindowRateLimiterOptions
                    {
                        PermitLimit = 100,
                        Window = TimeSpan.FromMinutes(1)
                    }));
            });

        return services;
    }
}