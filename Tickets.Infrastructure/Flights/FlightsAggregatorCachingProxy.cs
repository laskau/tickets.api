﻿using Microsoft.Extensions.Caching.Memory;
using Tickets.Domain.Flights;
using Tickets.Domain.Flights.DomainRequest;
using Tickets.Domain.Flights.Entities;

namespace Tickets.Infrastructure.Flights;

public class FlightsAggregatorCachingProxy(IFlightsAggregatorService origin, IMemoryCache cache) : IFlightsAggregatorService
{
    public async Task<Flight[]> GetAsync(GetFlightsDomainQuery query)
    {
        var cacheKey =
            $"FlightSearch_{query.DepartureAirport.IATACode}_{query.ArrivalAirport.IATACode}_{query.FlightDate:yyyy-MM-dd}";

        if (cache.TryGetValue(cacheKey, out Flight[] cachedFlights))
            return cachedFlights;

        var result = await origin.GetAsync(query);

        if (result != null && result.Length != 0)
        {
            cache.Set(cacheKey, result, TimeSpan.FromSeconds(5));
        }

        return result;
    }
}