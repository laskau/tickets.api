﻿using Microsoft.Extensions.Http.Resilience;
using Polly;
using System.Threading.RateLimiting;
using Tickets.Infrastructure.Flights.Providers.Amadeus;
using Tickets.Infrastructure.HttpUtils;

namespace Tickets.API.DependenciesRegistration;

public static class RegisterAmadeusDependencies
{
    public static IServiceCollection AddAmadeusDependencies(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddScoped<IAmadeusFlightsGateway, AmadeusFakeGatewayWithRandomDelay>()
            .AddHttpClient<AmadeusFlightsGateway>(client =>
            {
                client.BaseAddress = new Uri(configuration.GetValue<string>("AppSettings:AmadeusBaseUrl"));
                client.Timeout =
                    TimeSpan.FromMilliseconds(configuration.GetValue<int>("AppSettings:AmadeusTimeoutInMs"));
            })
            .AddHttpMessageHandler<RequestResponseMessagesLoggingHandler>()
            .AddResilienceHandler("AmadeusPipeline", pipelineBuilder =>
            {
                pipelineBuilder.AddCircuitBreaker(new HttpCircuitBreakerStrategyOptions()
                {
                    SamplingDuration = TimeSpan.FromSeconds(30),
                    FailureRatio = 0.2,
                    MinimumThroughput = 3,
                });

                pipelineBuilder.AddRateLimiter(new SlidingWindowRateLimiter(
                    new SlidingWindowRateLimiterOptions
                    {
                        PermitLimit = 100,
                        Window = TimeSpan.FromMinutes(1)
                    }));
            });

        return services;
    }
}