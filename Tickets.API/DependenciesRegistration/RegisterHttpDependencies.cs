﻿using Tickets.Infrastructure.HttpUtils;
using Tickets.Infrastructure.HttpUtils.RequestAndResponseLogEntries;

namespace Tickets.API.DependenciesRegistration;

public static class RegisterHttpDependencies
{
    public static IServiceCollection AddHttpDependencies(this IServiceCollection services)
    {
        services
            .AddSingleton<IHttpClientMessageLogEntryFactory, HttpClientMessageLogEntryFactory>()
            .AddTransient<RequestResponseMessagesLoggingHandler>();

        return services;
    }
}