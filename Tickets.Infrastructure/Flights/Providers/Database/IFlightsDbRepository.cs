﻿using Tickets.Domain.Flights.Entities;

namespace Tickets.Infrastructure.Flights.Providers.Database;

public interface IFlightsDbRepository
{
    Task<Flight[]> GetAsync(string departureCode, string arrivalCode, DateTime departureDateTime);
}