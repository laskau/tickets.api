﻿using Microsoft.AspNetCore.Mvc;
using Tickets.API.Application.Airport.Models;
using Tickets.Domain.Airports;

namespace Tickets.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AirportsController : ControllerBase
{
    [HttpGet]
    public async Task<IEnumerable<AirportModel>> GetAsync([FromServices] IAirportsRepository airportRepository)
    {
        var result = await airportRepository.GetAsync();
        return result.Select(x => new AirportModel() { Code = x.IATACode, Name = x.Name, Country = x.Country });
    }
}