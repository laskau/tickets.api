﻿namespace Tickets.Infrastructure.HttpUtils.RequestAndResponseLogEntries;

public sealed class HttpClientMessageLogEntryFactory : IHttpClientMessageLogEntryFactory
{
    public async Task<HttpRequestMessageLogEntry> CreateAsync(HttpRequestMessage requestMessage)
    {
        string requestContent = null;

        if (requestMessage.Content != null)
            requestContent = await requestMessage.Content.ReadAsStringAsync();

        return new HttpRequestMessageLogEntry
        {
            HttpMethod = requestMessage.Method.ToString(),
            Uri = requestMessage.RequestUri?.ToString(),
            Content = requestContent,
        };
    }

    public async Task<HttpResponseMessageLogEntry> CreateAsync(HttpResponseMessage responseMessage)
    {
        var responseContent = await responseMessage.Content.ReadAsStringAsync();

        return new HttpResponseMessageLogEntry
        {
            HttpMethod = responseMessage.RequestMessage?.Method.ToString(),
            Uri = responseMessage.RequestMessage?.RequestUri?.ToString(),
            StatusCode = (int)responseMessage.StatusCode,
            Content = responseContent,
        };
    }
}