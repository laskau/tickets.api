﻿namespace Tickets.API.Application.Flights.Requests;

public class GetFlightsRequest
{
    public string From { get; set; }
    public string To { get; set; }
    public DateTime Date { get; set; }
}