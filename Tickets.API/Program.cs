using FluentValidation;
using Tickets.API.DependenciesRegistration;
using Tickets.API.Middlewares;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddMemoryCache();
builder.Services.AddHttpDependencies();
builder.Services.AddAmadeusDependencies(builder.Configuration);
builder.Services.AddTravelPortDependencies(builder.Configuration);
builder.Services.AddAirportsDependencies(builder.Configuration);
builder.Services.AddFlightsDependencies(builder.Configuration);

builder.Configuration
    .AddJsonFile("appsettings.json", false, false)
    .AddJsonFile("Settings/aggregator-settings.json", false, true);

ValidatorOptions.Global.DefaultRuleLevelCascadeMode = CascadeMode.Stop;

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ApiRequestResponseLoggingMiddleware>();
app.MapControllers();
app.Run();
