﻿using FluentValidation;
using Tickets.Domain.Airports;

namespace Tickets.API.Application.Flights.Requests.Validators;
public sealed class GetFlightsRequestValidator : AbstractValidator<GetFlightsRequest>
{
    public GetFlightsRequestValidator(IAirportsRepository airportsRepository)
    {
        RuleFor(request => request.From)
            .NotEmpty()
            .WithMessage(request => $"{nameof(request.From)} is required")

            .MustAsync(async (from, _) =>
            {
                var airports = await airportsRepository.GetAsync();

                return airports.Select(x => x.IATACode).Contains(from.ToUpper());
            })
            .WithMessage(request => $"{request.From} is unknown IATA airport code");

        RuleFor(request => request.To)
            .NotEmpty()
            .WithMessage(request => $"{nameof(request.To)} is required")

            .MustAsync(async (to, _) =>
            {
                var airports = await airportsRepository.GetAsync();

                return airports.Select(x => x.IATACode).Contains(to.ToUpper());
            })
            .WithMessage(request => $"{request.To} is unknown IATA airport code");

        RuleFor(request => request.Date)
            .NotEmpty()
            .WithMessage(request => $"{nameof(request.Date)} is required");
    }
}
