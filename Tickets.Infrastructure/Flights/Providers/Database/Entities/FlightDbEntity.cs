﻿namespace Tickets.Infrastructure.Flights.Providers.Database.Entities;

public class FlightDbEntity
{
    public string FlightNumber { get; set; }
    public string DepartureIATACode { get; set; }
    public string ArrivalIATACode { get; set; }
    public DateTime DepartureDateTime { get; set; }
    public DateTime ArrivalDateTime { get; set; }
}