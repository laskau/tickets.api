﻿using Microsoft.Extensions.Caching.Memory;
using Tickets.Domain.Airports;
using Tickets.Infrastructure.Airports;

namespace Tickets.API.DependenciesRegistration;

public static class RegisterAirportsDependencies
{
    public static IServiceCollection AddAirportsDependencies(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<AirportsCsvRepository>();

        services.AddScoped<IAirportsRepository, AirportsRepositoryCachingProxy>(s =>
            new AirportsRepositoryCachingProxy(
                s.GetRequiredService<AirportsCsvRepository>(),
                s.GetRequiredService<IMemoryCache>()));

        return services;
    }
}