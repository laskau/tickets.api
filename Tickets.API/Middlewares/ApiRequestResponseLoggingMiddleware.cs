﻿using Microsoft.IO;

namespace Tickets.API.Middlewares
{
    public class ApiRequestResponseLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly RecyclableMemoryStreamManager _recyclableMemoryStreamManager;

        public ApiRequestResponseLoggingMiddleware(RequestDelegate next, ILogger<ApiRequestResponseLoggingMiddleware> logger)
        {
            _next = next;
            _recyclableMemoryStreamManager = new RecyclableMemoryStreamManager();
        }

        private static string ReadStreamInChunks(Stream stream)
        {
            const int readChunkBufferLength = 4096;
            stream.Seek(0, SeekOrigin.Begin);
            using var textWriter = new StringWriter();
            using var reader = new StreamReader(stream);
            var readChunk = new char[readChunkBufferLength];
            int readChunkLength;
            do
            {
                readChunkLength = reader.ReadBlock(
                    readChunk, 0, readChunkBufferLength);
                textWriter.Write(readChunk, 0, readChunkLength);
            } while (readChunkLength > 0);
            return textWriter.ToString();
        }

        public async Task Invoke(HttpContext context)
        {
            context.Request.EnableBuffering();
            await using var requestStream = _recyclableMemoryStreamManager.GetStream();
            await context.Request.Body.CopyToAsync(requestStream);
            var requestBodyAsText = ReadStreamInChunks(requestStream);

            Console.WriteLine($"Incoming request: {context.Request.Method} {context.Request.Scheme} {context.Request.Host} {context.Request.Path} {context.Request.QueryString} {requestBodyAsText}");

            context.Request.Body.Position = 0;

            var originalBodyStream = context.Response.Body;
            await using var responseBody = _recyclableMemoryStreamManager.GetStream();
            context.Response.Body = responseBody;

            await _next(context);

            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var responseBodyAsText = await new StreamReader(context.Response.Body).ReadToEndAsync();

            context.Response.Body.Seek(0, SeekOrigin.Begin);

            Console.WriteLine($"Outgoing response: {context.Response.StatusCode} {responseBodyAsText}");

            await responseBody.CopyToAsync(originalBodyStream);
        }
    }
}
