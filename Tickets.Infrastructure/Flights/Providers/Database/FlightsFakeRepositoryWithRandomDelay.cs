﻿using Tickets.Domain.Airports;
using Tickets.Domain.Airports.Entities;
using Tickets.Domain.Flights.Entities;

namespace Tickets.Infrastructure.Flights.Providers.Database;

public sealed class FlightsFakeRepositoryWithRandomDelay(IAirportsRepository airportsRepository) : IFlightsDbRepository
{
    public async Task<Flight[]> GetAsync(string departureCode, string arrivalCode, DateTime departureDateTime)
    {
        var airports = await airportsRepository.GetAsync();
        var departureDate = departureDateTime.AddHours(Random.Shared.Next(3, 5));

        await Task.Delay(TimeSpan.FromMilliseconds(Random.Shared.Next(200, 310)));

        return Enumerable.Range(1, Random.Shared.Next(5))
            .Select(_ => new Flight()
            {
                FlightNumber = $"DB-{Random.Shared.Next(9999, 99999)}",
                Departure = airports.FirstOrDefault(x => x.IATACode.Equals(departureCode)) ?? new NullAirport(),
                DepartureDate = departureDate,
                Arrival = airports.FirstOrDefault(x => x.IATACode.Equals(arrivalCode)) ?? new NullAirport(),
                ArrivalDate = departureDate.AddHours(Random.Shared.Next(5, 8))
            }).ToArray();
    }
}