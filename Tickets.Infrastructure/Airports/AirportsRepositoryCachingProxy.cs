﻿using Microsoft.Extensions.Caching.Memory;
using Tickets.Domain.Airports;
using Tickets.Domain.Airports.Entities;

namespace Tickets.Infrastructure.Airports;
public sealed class AirportsRepositoryCachingProxy(IAirportsRepository origin, IMemoryCache cache) : IAirportsRepository
{
    private string CacheKey => "Airports";
    public Task<Airport[]> GetAsync() => cache.GetOrCreateAsync(CacheKey, async _ => await origin.GetAsync());
}
