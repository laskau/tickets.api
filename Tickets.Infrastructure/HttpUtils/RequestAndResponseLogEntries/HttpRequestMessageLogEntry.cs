﻿namespace Tickets.Infrastructure.HttpUtils.RequestAndResponseLogEntries;

public sealed class HttpRequestMessageLogEntry
{
    public string HttpMethod { get; set; }
    public string Uri { get; set; }
    public string Content { get; set; }
}