﻿using Dapper;
using Microsoft.Data.SqlClient;
using Tickets.Domain.Airports;
using Tickets.Domain.Airports.Entities;
using Tickets.Domain.Flights.Entities;
using Tickets.Infrastructure.Flights.Providers.Database.Entities;

namespace Tickets.Infrastructure.Flights.Providers.Database;

public class FlightsDbRepository(string connectionString, IAirportsRepository airportsRepository) : IFlightsDbRepository
{
    public Flight Map(FlightDbEntity dbEntity, Airport[] airports) =>
        new()
        {
            FlightNumber = dbEntity.FlightNumber,
            Departure =
                airports.FirstOrDefault(a => a.IATACode.Equals(dbEntity.DepartureIATACode)) ?? new NullAirport(),
            DepartureDate = dbEntity.DepartureDateTime,
            Arrival =
                airports.FirstOrDefault(a => a.IATACode.Equals(dbEntity.ArrivalIATACode)) ?? new NullAirport(),
            ArrivalDate = dbEntity.ArrivalDateTime
        };

    public async Task<Flight[]> GetAsync(string departureCode, string arrivalCode, DateTime departureDateTime)
    {
        await using var sqlConnection = new SqlConnection(connectionString);
        await sqlConnection.OpenAsync();

        var procedureParams = new
        {
            DepCode = departureCode,
            ArrCode = arrivalCode,
            Date = departureDateTime
        };

        var result = await sqlConnection.QueryAsync<FlightDbEntity>("App.Flights_Get", procedureParams);

        var airports = await airportsRepository.GetAsync();
        return result.Select(r => Map(r, airports)).ToArray();
    }
}