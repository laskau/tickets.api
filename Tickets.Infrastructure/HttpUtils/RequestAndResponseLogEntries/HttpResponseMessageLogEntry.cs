﻿namespace Tickets.Infrastructure.HttpUtils.RequestAndResponseLogEntries;

public sealed class HttpResponseMessageLogEntry
{
    public string HttpMethod { get; set; }
    public string Uri { get; set; }
    public int StatusCode { get; set; }
    public string Content { get; set; }
}