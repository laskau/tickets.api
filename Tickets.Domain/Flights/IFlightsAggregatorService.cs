﻿using Tickets.Domain.Flights.DomainRequest;
using Tickets.Domain.Flights.Entities;

namespace Tickets.Domain.Flights;

public interface IFlightsAggregatorService
{
    Task<Flight[]> GetAsync(GetFlightsDomainQuery query);
}