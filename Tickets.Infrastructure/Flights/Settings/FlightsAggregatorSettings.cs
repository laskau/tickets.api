﻿namespace Tickets.Infrastructure.Flights.Settings;

public class FlightsAggregatorSettings
{
    public int TimeoutInMs { get; set; }

    public TimeSpan ServiceTimeout => TimeSpan.FromMilliseconds(TimeoutInMs);
}