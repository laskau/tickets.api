﻿namespace Tickets.Domain.Airports.Entities;

public sealed class NullAirport : Airport
{
    public override string IATACode => "Unknown";
}