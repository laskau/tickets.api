﻿using GeoTimeZone;
using System.Globalization;
using Tickets.Domain.Airports;
using Tickets.Domain.Airports.Entities;
using TimeZoneConverter;

namespace Tickets.Infrastructure.Airports;

public sealed class AirportsCsvRepository : IAirportsRepository
{
    private static string CsvPath => Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) +
                                     "/Airports/csv/airports.csv";
    private async Task<string[]> ReadHeaders()
    {
        using var reader = new StreamReader(CsvPath);

        var headersLine = await reader.ReadLineAsync();

        return !string.IsNullOrWhiteSpace(headersLine)
            ? headersLine.Replace("\"", "").Split(',')
            : [];
    }

    private async IAsyncEnumerable<string> ReadFileStream()
    {
        using var reader = new StreamReader(CsvPath);

        while (!reader.EndOfStream)
        {
            yield return await reader.ReadLineAsync();
        }
    }

    private Airport ParseCsvLine(Dictionary<string, int> headers, string line)
    {
        var tuple = line.Replace("\"", "").Split(',');
        var result = new Airport()
        {
            Name = tuple[headers["name"]],
            IATACode = tuple[headers["iata_code"]],
            City = tuple[headers["city"]],
            Country = tuple[headers["country"]],
            TimeZone = TimeZoneInfo.Utc
        };

        if (double.TryParse(tuple[headers["latitude"]], NumberStyles.Any, CultureInfo.InvariantCulture, out var lat) &&
            double.TryParse(tuple[headers["longitude"]], NumberStyles.Any, CultureInfo.InvariantCulture, out var lon))
        {
            var ianaTimezone = TimeZoneLookup.GetTimeZone(lat, lon).Result;
            result.TimeZone = TZConvert.GetTimeZoneInfo(ianaTimezone);
        }

        return result;
    }

    public async Task<Airport[]> GetAsync()
    {
        var headersLine = await ReadHeaders();
        var headers = new Dictionary<string, int>()
        {
            { "name", Array.IndexOf(headersLine, "name") },
            { "iata_code", Array.IndexOf(headersLine, "iata_code") },
            { "city", Array.IndexOf(headersLine, "municipality") },
            { "country", Array.IndexOf(headersLine, "iso_country") },
            { "latitude", Array.IndexOf(headersLine, "latitude_deg") },
            { "longitude", Array.IndexOf(headersLine, "longitude_deg") }
        };

        return await ReadFileStream()
            .Skip(1)
            .Where(line => line.Contains("large_airport"))
            .Select(line => ParseCsvLine(headers, line))
            .ToArrayAsync();
    }
}